<?php

class Obj {

    public $id;
    public $size;
    public $rong;  // store color object
    public $dim;

    function __construct($id, $size, $color, $dim) {
        $this->id = $id;
        $this->size = $size;
        $this->rong = $color;
        $this->dim = $dim;
    }


    /*
      function __clone() {
      $green = $this->rong->green;
      $blue = $this->rong->blue;
      $red = $this->rong->red;
      $this->color = unserialize(serialize($this->color)); //new Color($green, $blue, $red);
      
      $height = $this->dim->height;
      $width = $this->dim->width;
      
      $this->dim = new Dimension($width, $height);
      
      }
    */
    
    
    function __clone() {
        foreach ($this as $key => $val) {
            if (is_object($val) || (is_array($val))) {
                $this->{$key} = unserialize(serialize($val));
            }
        }
    }
      

}

class Color {

    public $green;
    public $blue;
    public $red;

    function __construct($green, $blue, $red) {
        $this->green = $green;
        $this->blue = $blue;
        $this->red = $red;
    }

}

class Dimension {
     public $width;
     public $height;
     
     
     public function __construct($width, $height) {
         $this->width = $width;
         $this->height = $height;
     }
}

$color = new Color(23, 42, 223);


$dim = new Dimension (50, 10);

$obj1 = new Obj(23, "small", $color, $dim);

$obj2 = clone $obj1;

// manually creating a clone
/*
foreach($obj1 as $k => $v) {
    $obj2->{$k} = $v;
}
*/

echo "<pre>";
print_r($obj1);
echo "<pre>";
print_r($obj2);




$obj2->id++;
$obj2->color->red = 255;
$obj2->size = "big";

$obj2->dim->height = 25;

echo "<pre>";
print_r($obj1);
echo "<pre>";
print_r($obj2);

