<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 interface payment {
     function showSuccessMessage();
    
     function showFailureMessage();
    
     function sendEmailToCustomer();
    
     function verifyPayment();
    
     function setGatewayIP();
    
     function setPaymentName();
    
     function setOrderId();
}


interface security {
    function setSecurityToken();
    function verifySecurityToken();
    function showSuccessMessage();
}

class bkash implements payment, security {
    
    
    function showSuccessMessage(){ }
    
     function showFailureMessage(){ }
    
     function sendEmailToCustomer(){ }
    
     function verifyPayment(){ }
    
     function setGatewayIP(){ }
    
     function setPaymentName(){ }
    
     function setOrderId(){ }
     
     function setSecurityToken(){ }
 
     function verifySecurityToken(){ }

     
}